from fabric.api import task, env, local, put, cd, sudo
from tempfile import mkdtemp
from shutil import rmtree
from os.path import join

env.host_string = "pyconde00.gocept.net"
env.sudo_user = "pyconde"
env.target_dir = '/srv/pyconde/env_pyconde_2013/htdocs'


@task
def deploy():
    """
    Given gocept's current system structure we first upload the files into
    a directory that can be used by the current user and are then extracted
    into the final directory as the final service user.
    """
    tmpdir = mkdtemp()
    target_file = join(tmpdir, '2013.tar.bz2')
    try:
        local('lessc -x static/css/main.less > static/css/main.css')
        local('tar --exclude=".git*" --exclude="*.pxm" --exclude="fabfile*"'
            ' --exclude=".DS_Store" --exclude="background.png"'
            ' --exclude="._*" --exclude="logo.png"'
            ' --exclude="*.less" -cjvf {0} .'.format(target_file))
        put(target_file, "~/")
        with cd(env.target_dir):
            sudo('tar -xjvf ~{user}/2013.tar.bz2'.format(**env), user=env.sudo_user)
    finally:
        rmtree(tmpdir)
